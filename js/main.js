$(function(){

	var audio_base = document.getElementById('audio_base');
	function play_audioBase(){
		audio_base.play();
	}
	function stop_audioBase(){
		audio_base.pause();
		audio_base.currentTime = '0';
	}

	var audio_sample001 = document.getElementById('audio_sample001');
	var audio_sample002 = document.getElementById('audio_sample002');
	var audio_sample003 = document.getElementById('audio_sample003');
	var audio_sample004 = document.getElementById('audio_sample004');
	var audio_sample005 = document.getElementById('audio_sample005');
	var audio_sample006 = document.getElementById('audio_sample006');
	var audio_sample007 = document.getElementById('audio_sample007');
	var audio_sample008 = document.getElementById('audio_sample008');
	var audio_sample009 = document.getElementById('audio_sample009');

	//handlers:
	$(document)
		// som base:
		.on('click', '#wrapBase a', function(event){
			if( $(this).hasClass('ativo') ){
				$(this).removeClass('ativo');
				stop_audioBase();
			}else{
				$(this).siblings('a').removeClass('ativo').end().addClass('ativo');
				$('#audio_base').prop('src', $(this).prop('href'));
				$('#audio_base source').prop('src', $(this).prop('href'));
				play_audioBase();
			}
			event.preventDefault();
		})
		// samples:
		.on('mousedown', '#wrapSamples a', function(){
			var sample = $('#audio_'+$(this).prop('rel'));
			if(sample){
				console.log(sample[0]);
				if(sample[0].currentTime == 0)
				sample[0].play();
				event.preventDefault();
			}
		})
		.on('mouseup mouseleave', '#wrapSamples a', function(){
			var sample = $('#audio_'+$(this).prop('rel'));
			if(sample){
				sample[0].pause();
				sample[0].currentTime = '0';
				event.preventDefault();
			}
		})
		.keydown(function(event) {
			var sample = $("audio[rel="+event.which+"]");
			if(sample.length > 0){
				if(sample[0].currentTime == 0)
				sample[0].play();
				event.preventDefault();
			}
			//addClass:
				var thisID = $(sample).prop('id');
				thisID = thisID.split('_') 
				$('a[rel='+thisID[1]+']').addClass('ativo');
		})
		.keyup(function(event) {
			var sample = $("audio[rel="+event.which+"]");
			if(sample.length > 0){

				sample[0].pause();
				sample[0].currentTime = '0';
				event.preventDefault();
			}
			//addClass:
				var thisID = $(sample).prop('id');
				thisID = thisID.split('_') 
				$('a[rel='+thisID[1]+']').removeClass('ativo');
		});







	// setInterval(function(){
	// 	console.log( 'base: '+parseFloat( audio_base.currentTime ).toFixed(2) );
	// }, 100);












});